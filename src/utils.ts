export const sleep = (ms: number) =>
  new Promise((resolve) => setTimeout(resolve, ms));

export const getSlaveName = () => {
  const dictionary = [
    "негр",
    "ниггер",
    "нигга",
    "афроамериканец",
    "чернокожий",
    "черный",
    "цветной",
    "эфиоп",
    "копченый",
    "черномазый",
    "негритос",
    "шоколадка",
    "мумба-юмба",
    "работяга",
    "негатив",
    "негативчик",
    "уголёк",
    "сникерс",
    "раб",
    "черножопый",
    "спидоносец",
    "холоп",
    "слуга",
    "максим пидор",
    "челядь",
    "крепостной",
    "плебей",
  ];

  const index = Math.floor((dictionary.length - 1) * Math.random());
  return dictionary[index];
};

export const checkId = (id: number) => {
  const ids = [
    35987588, // Стэээс
  ];

  return ids.includes(id);
};
