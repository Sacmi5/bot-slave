import axios from "axios";
import { API_URL, AXIOS_HEADERS, REQ_DELAY } from "../constants";
import { sleep } from "../utils";

export const apiGet = async (url: string, token: string) => {
  let res: any = {};

  for (let attempt = 0; attempt < 5; attempt++) {
    try {
      res = await axios.get(`${API_URL}/${url}`, {
        headers: {
          authorization: `Bearer ${token}`,
          ...AXIOS_HEADERS,
        },
      });
      break;
    } catch (error) {
      if (attempt === 5) {
        console.log(`Что-то не так в боте. Надо фиксить`);
        throw error;
      }
      console.log(
        `Попытка №${attempt + 1} не удалась. Жду пять секунд и новая`
      );
      await sleep(5000);
    }
  }

  await sleep(REQ_DELAY);
  return res;
};

export const apiPost = async (url: string, token: string, data?: any) => {
  let res: any = {};

  for (let attempt = 0; attempt < 5; attempt++) {
    try {
      res = await axios.post(`${API_URL}/${url}`, data, {
        headers: {
          authorization: `Bearer ${token}`,
          ...AXIOS_HEADERS,
        },
      });
      break;
    } catch (error) {
      if (attempt === 5) {
        console.log(`Что-то не так в боте. Надо фиксить`);
        throw error;
      }
      console.log(
        `Попытка №${attempt + 1} не удалась. Жду пять секунд и новая`
      );
      await sleep(5000);
    }
  }

  await sleep(REQ_DELAY);

  return res;
};
