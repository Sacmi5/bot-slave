import * as scheduler from "simple-scheduler-task";
import { config } from "dotenv";

import Account from "./class/account";
import { INTERVAL_MS } from "./constants";

const token = process.env.TOKEN ?? config().parsed?.TOKEN;

if (!token) {
  console.error("Токен укажи, ебло (в .env или в переменных)");
  process.exit(-1);
}

const account = new Account(token);

new scheduler.Interval(async () => {
  console.log("------- Новый цикл -------");
  await account.updateInfo();
  await account.checkSlaves();
  console.log("------- Конец цикла -------");
}, INTERVAL_MS);

console.log(`ждем ${INTERVAL_MS / 1000} сек. и работаем BloodTrail`);
