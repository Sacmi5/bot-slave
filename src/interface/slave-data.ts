export interface SlaveData {
  id: number;
  job: string;
  profit: number;
  sale_price: number;
  fetter_price: number;
  fetter_to: Date;
}
